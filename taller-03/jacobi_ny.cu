#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define I(stride,i,j) (i)*(stride)+(j) //row-major order

// initialization
void initData(int ny, int nx, float *temp)
{
  int i,j;
  for( i=0; i<nx; i++ )
    for( j=0; j<ny; j++ )
      temp[i*ny+j] = 0.0;

  i=0;// set left wall to 1
  for( j=0; j<ny; j++ )
    temp[i*ny+j]=1.0;  
}

__global__ 
void Laplace(int n1, int n2, float *Told, float *Tnew) 
{
  int j = blockIdx.x*blockDim.x + threadIdx.x; //n1
  int i = blockIdx.y*blockDim.y + threadIdx.y; //n2
  // only update "interior" node points
  if(i>0 && i<n2-1 && j>0 && j<n1-1) {
    Tnew[I(n1,i,j)] = 0.25*( Told[I(n1,i+1,j)] + 
                             Told[I(n1,i-1,j)] + 
                             Told[I(n1,i,j+1)] + 
                             Told[I(n1,i,j-1)] );
  }
}

//plot auxiliar functions
void gnuPlot(int ny,int nx, char *bname, char *fplot, char *title);
void wrtData(char *fname, size_t N, float *dat);

int main(int argc,char* argv[])
{
    int Ny=130;    //y-axis dimension, mesh size 
    int Nx=151;    //x-axis dimension, mesh size 
    int Tout=2000;  //total iterations
    int nBytes = Ny*Nx*sizeof(float); //Bytes-size in memory

    //array GPU-memory 
    float *dT1=NULL;
    float *dT2=NULL;  

    // allocate T array on the host
    float *T = (float *)malloc(nBytes);

    // initialize array on the host
    initData(Ny,Nx,T);

    // allocate storage space on the GPU
    cudaMalloc( &dT1,nBytes );
    cudaMalloc( &dT2,nBytes );

    // copy (initialized) host arrays to the GPU memory from CPU memory
    cudaMemcpy(dT1,T,nBytes,cudaMemcpyHostToDevice);
    cudaMemcpy(dT2,T,nBytes,cudaMemcpyHostToDevice);

    // assign a 2D distribution of CUDA "threads" within each CUDA "block"  
    int ThrdBlck_Y = 16;
    int ThrdBlck_X = 16;
  
    // calculate number of blocks along X and Y in a 2D CUDA "grid"
    dim3 dimBlock(ThrdBlck_Y,ThrdBlck_X);
    dim3 dimGrid((Ny-1)/ThrdBlck_Y+1,(Nx-1)/ThrdBlck_X+1);

    // begin Jacobi iteration
    for (int k=0; k<Tout; k+=2 )
    {
      Laplace <<<dimGrid,dimBlock>>> (Ny,Nx,dT1,dT2);// update T1 using data stored in T2
      Laplace <<<dimGrid,dimBlock>>> (Ny,Nx,dT2,dT1);// update T2 using data stored in T1
    }

    // copy final array to the CPU from the GPU 
    cudaMemcpy(T,dT2,nBytes,cudaMemcpyDeviceToHost);

    //write binary data
    wrtData((char*)"data.raw",Ny*Nx,T);

    //call gnuplot 
    gnuPlot(Ny,Nx,(char*)"data.raw",(char*)"plot.gpi",(char*)"view");
    
    // release memory on the host 
    free(T);

    // release memory on the device 
    cudaFree(dT1);
    cudaFree(dT2);

    return (0);
}

void gnuPlot(int ny,int nx, char *bname, char *fplot, char *title){
  FILE *outf=fopen(fplot,"w");
  fprintf(outf, "set pm3d map\n");
  fprintf(outf, "set size ratio %0.2f\n",(float)ny/nx);
  fprintf(outf, "set grid\n");
  fprintf(outf, "set yrange [0:%d] reverse \n",ny-1);
  fprintf(outf, "set xrange [0:%d]\n",nx-1);
  fprintf(outf, "set xlabel '[pixels]'\n");
  fprintf(outf, "set ylabel '[pixels]'\n");
  fprintf(outf, "set title '%s'\n",title);
  fprintf(outf, "splot '%s' binary array=%dx%d format='%sfloat' \
scan=yx t 'f(x,z)'\n",bname,nx,ny,"%");
  fprintf(outf, "pause -1 'Do you close..?'\n");
  fclose(outf);
  system("gnuplot plot.gpi");
}

void wrtData(char *fname, size_t N, float *dat){
  FILE *outfile=fopen( fname,"wb" ); 
  fwrite(dat,sizeof(float),N,outfile);
  fclose(outfile);
}
