#include<stdlib.h>
#include<stdio.h>
#include<math.h>
typedef double (*funPtr)(double);
void initData(funPtr fun,int n,float *v){
  int i;
  for(i=0;i<n;i++)
    v[i]=fun(i)*fun(i);
}
__global__
void addVec(int n,float *a,float *b,float *c){
  //Just to write here!
}
int main(){
  int i;
  int nElem = 1e6;
  int nBytes = nElem*sizeof(float);

  float  *a=NULL, *b=NULL, *c=NULL;//host varible
  float *a_d=NULL,*b_d=NULL,*c_d=NULL;//device variable
 
  //Host-memory allocate array 
  a = (float*)malloc(nBytes);
  b = (float*)malloc(nBytes);
  c = (float*)malloc(nBytes);

  // allocate memory on device 
	cudaMalloc(&a_d, nBytes);
	cudaMalloc(&b_d, nBytes);
	cudaMalloc(&c_d, nBytes);
  
  initData(sin,nElem,a);
  initData(cos,nElem,b);

  //copy infomation from host to device
  cudaMemcpy(a_d, a, nBytes, cudaMemcpyHostToDevice);
  cudaMemcpy(b_d, b, nBytes, cudaMemcpyHostToDevice);

  int threadsBlock = 256;//warp size
  int nBlocks;
  nBlocks = (nElem-1)/threadsBlock + 1;

  //kernel parallel summation on GPU
  addVec<<<nBlocks,threadsBlock>>>(nElem,a_d,b_d,c_d);

  //copy infomation from device to host
  cudaMemcpy(c, c_d, nBytes, cudaMemcpyDeviceToHost);

  for(i=0;i<nElem;i++){
    if (c[i] != 1.0) {
      printf("The data in [%d] doesn't match\n",i);
    }
  }

  free(a);
  free(b);
  free(c);
  
  cudaFree(a_d);
  cudaFree(b_d);
  cudaFree(c_d);

  return(0);
}
  
