#include<stdlib.h>
#include<stdio.h>
#include<math.h>
typedef double (*funPtr)(double);
void initData(funPtr fun,int n,float *v){
  int i;
  for(i=0;i<n;i++)
    v[i]=fun(i)*fun(i);
}  
void addVec(int n,float *a,float *b,float *c){
  int i;
  for(i=0;i<n;i++)
    c[i]=a[i]+b[i];
}
int main(){
  int i;
  int nElem=1e6;
  int nBytes = nElem*sizeof(float);

  float *a=NULL,*b=NULL,*c=NULL;
 
  //Host-memory allocate array 
  a = (float*)malloc(nBytes);
  b = (float*)malloc(nBytes);
  c = (float*)malloc(nBytes);
  
  initData(sin,nElem,a);
  initData(cos,nElem,b);

  addVec(nElem,a,b,c);

  /*
  for(i=0;i<nElem;i++)
    printf("%0.5f + %0.5f = %0.1f\n",a[i],b[i],c[i]);
  */
    
  for(i=0;i<nElem;i++){
    if (c[i] != 1.0) {
      printf("The data in [%d] doesn't match\n",i);
    }
  }

  free(a);
  free(b);
  free(c);
  return(0);
}
  
