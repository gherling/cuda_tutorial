#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <helper_cuda.h>

// Kernel function to add the elements of two arrays
__global__
void add(int n, float *x, float *y) {
  int k;
  int  index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  
  for ( k = index; k < n; k += stride)
    y[k] = x[k] + y[k];
}
void deviceInfo(void); 
int getSM2Cores(cudaDeviceProp);
int getSPcores(cudaDeviceProp);
int main(void)
{
  int i,N = 1<<25;
  int blockSize = 32*32; //factor de 32 (tamagno de warps)
  int numBlocks = (N-1)/blockSize+1;
  
  deviceInfo();

  float *x, *y;

  // Allocate Unified Memory – accessible from CPU or GPU
  cudaMallocManaged(&x, N*sizeof(float));
  cudaMallocManaged(&y, N*sizeof(float));

  // initialize x and y arrays on the host
  for (int i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }

  // Run kernel on 1M elements on the GPU
  add<<<numBlocks,blockSize>>>(N, x, y);

  // Wait for GPU to finish before accessing on host
  cudaDeviceSynchronize();

  // Check for errors (all values should be 3.0f)
  float maxError = 0.0f;
  for ( i = 0; i < N; i++)
    maxError = fmax(maxError, fabs(y[i]-3.0f));
  fprintf(stdout,"Max error: %f\n", maxError);

  // Free memory
  cudaFree(x);
  cudaFree(y);
  
  return (0);
}

void deviceInfo() {
  cudaDeviceProp prop;
  int i,count;
  cudaGetDeviceCount (&count);
  for (i=0;i<count;i++) {
    cudaGetDeviceProperties (&prop,i);
    fprintf(stdout,"------------------------------\n");
    fprintf(stdout,"Device: %d\n", i );
    fprintf(stdout,"Name: %s\n", prop.name );
    fprintf(stdout,"Compute capability: %d.%d\n", prop.major, prop.minor );
    fprintf(stdout,"Clock rate: %d\n", prop.clockRate );
    fprintf(stdout,"GPU global mem: %ld MB\n", prop.totalGlobalMem/1024/1024 );
    fprintf(stdout,"Multiprocessor count: %d\n", prop.multiProcessorCount );
//  fprintf(stdout,"CUDA Cores: %d\n", getSPcores(prop) );
    fprintf(stdout,"CUDA Cores: %d\n", getSM2Cores(prop) );
    fprintf(stdout,"Shared mem per mp: %ld KB\n", prop.sharedMemPerBlock/1024 );
    fprintf(stdout,"Registers per mp: %d\n", prop.regsPerBlock );
    fprintf(stdout,"Threads in warp: %d\n", prop.warpSize );
    fprintf(stdout,"Max threads per block: %d\n",prop.maxThreadsPerBlock );
    fprintf(stdout,"Max thread dimensions: (%d, %d, %d)\n", \
        prop.maxThreadsDim[0], prop.maxThreadsDim[1],prop.maxThreadsDim[2] );
    fprintf(stdout,"Max grid dimensions: (%d, %d, %d)\n",   \
        prop.maxGridSize[0], prop.maxGridSize[1],prop.maxGridSize[2] );
    fprintf(stdout,"------------------------------\n");
  }
}
/*
   In the CUDA 6.0 samples there is a library "helper_cuda.h" which contains a routine
   _ConvertSMVer2Cores(int major, int minor) which takes the compute capability level
   of the GPU and returns the number of cores (stream processors) in each SM or SMX
   You then have to multiply this by the number of stream multiprocessors in your GPU.
   This can be found via cudaGetDeviceProperties().
   The CUDA 6.0 samples deviceQuery.cpp has example code to do this:
*/
int getSM2Cores(cudaDeviceProp devProp){
  return _ConvertSMVer2Cores(devProp.major, devProp.minor) * devProp.multiProcessorCount;
}
/*
  The cores per multiprocessor is the only "missing" piece of data. That data is not 
  provided directly in the cudaDeviceProp structure, but it can be inferred based on 
  published data and more published data from the devProp.major and devProp.minor 
  entries, which together make up the CUDA compute capability of the device.
*/   
int getSPcores(cudaDeviceProp devProp){
  int cores = 0;
  int mp = devProp.multiProcessorCount;
  switch (devProp.major){
     case 2: // Fermi
      if (devProp.minor == 1) cores = mp * 48;
      else cores = mp * 32;
      break;
     case 3: // Kepler
      cores = mp * 192;
      break;
     case 5: // Maxwell
      cores = mp * 128;
      break;
     case 6: // Pascal
     if (devProp.minor == 1) cores = mp * 128;
     else if (devProp.minor == 0) cores = mp * 64;
     else printf("Unknown device type\n");
     break;
     case 7: // Volta
     if (devProp.minor == 0) cores = mp * 64;
     else printf("Unknown device type\n");
     break;
     default:
     printf("Unknown device type\n"); 
     break;
  }
  return cores;
}
