#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#define J(i,j) (i)*n1+(j) //row-major order

// Three-layer velocity model
void initVel(int n1, int n2,float cmin,float cmax, float *vel)
{
  int i,j;
  float media = 0.5*(cmax+cmin);
  for( i=0; i<n2; i++ )
  {
    for( j=0;      j<n1/3;   j++ ) vel[J(i,j)] = cmin;
    for( j=n1/3;   j<2*n1/3; j++ ) vel[J(i,j)] = media;
    for( j=2*n1/3; j<n1;     j++ ) vel[J(i,j)] = cmax;
  }

}

__global__ 
void update(int iter,   //time step 
            int n1,     //first dimension
            int n2,     //second dimension
            int sy,     //y-index source position 
            int sx,     //x-index source position 
            float dt,   //time step size
            float dh,   //grid size
            float *wav, //disturbed source array
            float *vel, //velocity media
            float *p1,  //previous p-field
            float *p2)  //current p-field
{
  float G;
  
  int j = blockIdx.x*blockDim.x + threadIdx.x + 1; //n1
  int i = blockIdx.y*blockDim.y + threadIdx.y + 1; //n2   
  

  if (i<n2-1 && j<n1-1) {
    G = vel[J(i,j)]*dt/dh;
    p1[J(i,j)] = (2-4*G*G)*p2[J(i,j)] - p1[J(i,j)] +
                      G*G*( p2[ J(i+1, j) ] + p2[ J(i-1,j) ] +
                            p2[ J(i, j+1) ] + p2[ J(i,j-1) ] ); 
  }
  
  // add source perturbation 
  if(i==sx && j==sy)
    p1[J(sx,sy)] += wav[iter];
  
}

__global__
void source(int nt, float dt, float fq, float *wav){
  size_t i = threadIdx.x+blockDim.x*blockIdx.x;
  float arg; 
  if (i<nt) 
  {
    arg = M_PI*fq*fabsf(i*dt-1.0/fq);
    arg *= arg;
    wav[i] = (1-2*arg)*expf(-arg);// ricker wavelet
  }
}

void swapper(float* a[],float* b[]){
  float *tmp=*a;
  *a = *b;
  *b = tmp;
}

//plot auxiliar functions
void gnuPlot(int ny,int nx,float dh,char *bname, char *fplot, char *title);
void wrtData(char *fname, size_t N, float *dat);

int main(int argc,char* argv[])
{
    int nz=450;     //y-axis dimension, mesh size 
    int nx=651;     //x-axis dimension, mesh size 
    int Tout=800;   //total iterations
    int ISy = 4; //x-index source
    int ISx = nx/2; //x-index source
    int nBytes = nz*nx*sizeof(float); //Bytes-size in memory

    float Vmax = 3.0;       //[Km/s]
    float Vmin = 1.7;       //[Km/s]
    float dh = 0.01;        //[Km]
    float fq = Vmin/(20*dh); //[Hz]
    float dt = dh/(Vmax*sqrt(2)); //[s]

    //array GPU-memory 
    float *dP1 = NULL;
    float *dP2 = NULL;  
    float *dvel= NULL;  
    float *dwav= NULL;  

    // allocate arrays on the host
    float *tmp = (float*) malloc (nBytes);

    // initialize array on the host
    initVel(nz,nx,Vmin,Vmax,tmp);//velocity
    
    //test
    //wrtData((char*)"data.raw",nz*nx,tmp);

    // allocate storage space on the GPU
    cudaMalloc( &dP1, nBytes );
    cudaMalloc( &dP2, nBytes );
    cudaMalloc( &dvel,nBytes );
    cudaMalloc( &dwav,Tout*sizeof(float) );

    // copy (initialized) host arrays to the GPU memory from CPU memory
    cudaMemcpy(dvel,tmp,nBytes,cudaMemcpyHostToDevice);

    //Defines the disturbed source in gpu-array 
    source<<<(Tout-1)/256+1,256>>>(Tout,dt,fq,dwav);

    dim3 blockSize(32,32); //Max threads per block (32 X 32 = 1024) 
    dim3  gridSize((nz-1)/blockSize.x+1,(nx-1)/blockSize.y+1);

    // begin time loop
    for (int k=0; k<Tout; k++ )
    {
      update <<<gridSize,blockSize>>> (k,nz,nx,ISy,ISx,dt,dh,dwav,dvel,dP1,dP2);
      swapper(&dP1,&dP2);
    }
    // End time loop

    // copy final array to the CPU from the GPU 
    cudaMemcpy(tmp,dP1,nBytes,cudaMemcpyDeviceToHost);

    //write binary data
    wrtData((char*)"data.raw",nz*nx,tmp);

    //call gnuplot 
    gnuPlot(nz,nx,dh,(char*)"data.raw",(char*)"plot.gpi",(char*)"wave field");
    
    // release memory on the host 
    free(tmp);

    // release memory on the device 
    cudaFree(dP1);
    cudaFree(dP2);
    cudaFree(dvel);
    cudaFree(dwav);

    return (0);
}

void gnuPlot(int ny,int nx, float dh, char *bname, char *fplot, char *title){
  FILE *outf=fopen(fplot,"w");
  fprintf(outf, "set pm3d map\n");
  fprintf(outf, "set size ratio %0.2f\n",(float)ny/nx);
  fprintf(outf, "set grid\n");
  fprintf(outf, "set palette rgbformulae 34,13,10 \n");
  fprintf(outf, "set yrange [%0.3f:0]\n",(ny-1)*dh);
  fprintf(outf, "set xrange [0:%0.3f]\n",(nx-1)*dh);
  fprintf(outf, "set xlabel 'Coordinates [Km]'\n");
  fprintf(outf, "set ylabel 'Depth [Km]'\n");
  fprintf(outf, "set title '%s'\n",title);
  fprintf(outf, "splot '%s' binary array=%dx%d dx=%0.2f dy=%0.2f \
  format='%sfloat 'scan=yx  t 'f(x,z)'\n",bname,nx,ny,dh,dh,"%");
  fprintf(outf, "pause -1 'Do you close..?'\n");
  fclose(outf);
  system("gnuplot plot.gpi");
}

void wrtData(char *fname, size_t N, float *dat){
  FILE *outfile=fopen( fname,"wb" ); 
  fwrite(dat,sizeof(float),N,outfile);
  fclose(outfile);
}
