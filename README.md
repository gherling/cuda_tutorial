# cuda_tutorial

El siguiente es un taller realizado en el agno de 2019. Su objeto es proporcinar una introduccion al desarrollo de computo de lato desempegno aplicado al modelado sismico.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/gherling/cuda_tutorial.git
git branch -M main
git push -uf origin main
```

## License
This work is licensed under the GNU General Public License (GPL) v3.0. See the
`LICENSE` file for more details. 

## Author

You can contact me to email adress:
* Herling Gonzalez Alvarez: <gherling@gmail.com>
