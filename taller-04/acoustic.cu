#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#define J(stride,i,j) (i)*(stride)+(j) //row-major order

// Three-layer velocity model
void initVel(int ny, int nx,float cmin,float cmax, float *vel)
{
  int i,j;
  float media = 0.5*(cmax+cmin);
  for( i=0; i<nx; i++ )
  {
    for( j=0;      j<ny/3;   j++ ) vel[J(ny,i,j)] = cmin;
    for( j=ny/3;   j<2*ny/3; j++ ) vel[J(ny,i,j)] = media;
    for( j=2*ny/3; j<ny;     j++ ) vel[J(ny,i,j)] = cmax;
  }

}

__global__ 
void update(int iter,   //time step 
            int n1,     //first dimension
            int n2,     //second dimension
            int sy,     //y-index source position 
            int sx,     //x-index source position 
            float dt,   //time step size
            float dh,   //grid size
            float *wav, //disturbed source array
            float *vel, //velocity media
            float *p1,  //previous p-field
            float *p2)  //current p-field
{
  int j = blockIdx.x*blockDim.x + threadIdx.x + 1; //n1
  int i = blockIdx.y*blockDim.y + threadIdx.y + 1; //n2
  // only update "interior" node points
  if( i<n2-1 && j<n1-1) 
  {
           float G = vel[J(n1,i,j)]*dt/dh;
           p1[J(n1,i,j)] = (2-4*G*G)*p2[J(n1,i,j)] - p1[J(n1,i,j)] +
                      G*G*( p2[ J(n1,i+1, j) ] + p2[ J(n1,i-1,j) ] +
                            p2[ J(n1,i, j+1) ] + p2[ J(n1,i,j-1) ] ); 
  }
  // add source perturbation 
  if(i==sx && j==sy)
    p1[J(n1,sx,sy)] += wav[iter];
  
}

__global__
void source(int nt, float dt, float fq, float *wav){
  int i = threadIdx.x+blockDim.x*blockIdx.x;
  float arg; 
  if (i<nt) 
  {
    arg = M_PI*fq*fabsf(i*dt-1.0/fq);
    arg *= arg;
    wav[i] = (1-2*arg)*expf(-arg);// ricker wavelet
  }
}

void swapper(float* a[],float* b[]){
  float *tmp=*a;
  *a = *b;
  *b = tmp;
}

//plot auxiliar functions
void gnuPlot(int ny,int nx, char *bname, char *fplot, char *title);
void wrtData(char *fname, size_t N, float *dat);

int main(int argc,char* argv[])
{
    int Ny=250;     //y-axis dimension, mesh size 
    int Nx=251;     //x-axis dimension, mesh size 
    int Tout=300;   //total iterations
    int ISy = Ny/3; //x-index source
    int ISx = Nx/2; //x-index source
    int nBytes = Ny*Nx*sizeof(float); //Bytes-size in memory

    float Vmax = 3000;       //[m/s]
    float Vmin = 1000;       //[m/s]
    float dh = 10.0;         //[m]
    float fq = Vmin/(20*dh); //[Hz]
    float dt = dh/(Vmax*sqrt(2)); //[s]

    //array GPU-memory 
    float *dP1 = NULL;
    float *dP2 = NULL;  
    float *dvel= NULL;  
    float *dwav= NULL;  

    // allocate arrays on the host
    float *vel = (float *)malloc(nBytes);
    float *P   = (float *)malloc(nBytes);

    // initialize array on the host
    initVel(Ny,Nx,Vmin,Vmax,vel);

    // allocate storage space on the GPU
    cudaMalloc( &dP1, nBytes );
    cudaMalloc( &dP2, nBytes );
    cudaMalloc( &dvel,nBytes );
    cudaMalloc( &dwav,Tout*sizeof(float) );

    // copy (initialized) host arrays to the GPU memory from CPU memory
    cudaMemcpy(dvel,vel,nBytes,cudaMemcpyHostToDevice);

    //Defines the disturbed source in gpu-array 
    source<<<(Tout-1)/256+1,256>>>(Tout,dt,fq,dwav);

    dim3 blockSize(32,32); //Max threads per block (32 X 32 = 1024) 
    dim3  gridSize((Ny-1)/blockSize.x+1,(Nx-1)/blockSize.y+1);

    // begin time loop
    for (int k=0; k<Tout; k++ )
    //for (int k=0; k<Tout; k+=2 )
    {
      update <<<gridSize,blockSize>>> (k,Ny,Nx,ISy,ISx,dt,dh,dwav,dvel,dP1,dP2);
      //update <<<gridSize,blockSize>>> (k,Ny,Nx,ISy,ISx,dt,dh,dwav,dvel,dP2,dP1);
      swapper(&dP1,&dP2);
    }
    // End time loop

    // copy final array to the CPU from the GPU 
    cudaMemcpy(P,dP1,nBytes,cudaMemcpyDeviceToHost);

    //write binary data
    wrtData((char*)"data.raw",Ny*Nx,P);

    //call gnuplot 
    gnuPlot(Ny,Nx,(char*)"data.raw",(char*)"plot.gpi",(char*)"view");
    
    // release memory on the host 
    free(P);
    free(vel);

    // release memory on the device 
    cudaFree(dP1);
    cudaFree(dP2);
    cudaFree(dvel);
    cudaFree(dwav);

    return (0);
}

void gnuPlot(int ny,int nx, char *bname, char *fplot, char *title){
  FILE *outf=fopen(fplot,"w");
  fprintf(outf, "set pm3d map\n");
  fprintf(outf, "set size ratio %0.2f\n",(float)ny/nx);
  fprintf(outf, "set grid\n");
  fprintf(outf, "set palette rgbformulae 33,14,10 \n");
  fprintf(outf, "set yrange [%d:0]\n",ny-1);
  fprintf(outf, "set xrange [0:%d]\n",nx-1);
  fprintf(outf, "set xlabel '[pixels]'\n");
  fprintf(outf, "set ylabel '[pixels]'\n");
  fprintf(outf, "set title '%s'\n",title);
  fprintf(outf, "splot '%s' binary array=%dx%d format='%sfloat' \
scan=yx t 'f(x,z)'\n",bname,nx,ny,"%");
  fprintf(outf, "pause -1 'Do you close..?'\n");
  fclose(outf);
  system("gnuplot plot.gpi");
}

void wrtData(char *fname, size_t N, float *dat){
  FILE *outfile=fopen( fname,"wb" ); 
  fwrite(dat,sizeof(float),N,outfile);
  fclose(outfile);
}
