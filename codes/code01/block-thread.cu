#include <stdio.h>
__global__
void getGlobalIdx(void) {
  int bIdx_x = blockIdx.x;
  int bDim_x = blockDim.x;
  int thIdx_x = threadIdx.x;
  int index = bIdx_x * bDim_x + thIdx_x;
  printf("(blockIdx.x=%d, blockDim.x=%d, threadIdx.x=%d) = %d\n", \
      bIdx_x,bDim_x,thIdx_x,index);
}
__global__
void getGlobalIdx2(int n){
  int bIdx_x = blockIdx.x;
  int bDim_x = blockDim.x;
  int thIdx_x = threadIdx.x;
  int index = bIdx_x * bDim_x + thIdx_x;
  if (index < n) {
  printf("(blockIdx.x=%d, blockDim.x=%d, threadIdx.x=%d) = %d\n", \
      bIdx_x,bDim_x,thIdx_x,index);
  }
}
int main() {    
  
  int N = 32;
  int threadBlock = 8;
  int numBlocks = (N-1)/threadBlock+1;

  getGlobalIdx <<<numBlocks,threadBlock>>>();
  cudaDeviceSynchronize();

  printf("-------------------------------------\n");
  
  getGlobalIdx2 <<<numBlocks,threadBlock>>>(N);
  cudaDeviceSynchronize();
  
  printf("NumData=%d, numBlocks=%d, threadBlock=%d\n",N,numBlocks,threadBlock);

  return(0);
}
