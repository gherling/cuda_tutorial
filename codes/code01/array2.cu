#include<valarray>
#include<cstdlib>
#include<cstdio>
#include<cmath>
typedef double (*funPtr)(double);
void initData(funPtr fun,unsigned n,float *v){
  unsigned i;
  for(i=0;i<n;i++)
    v[i]=fun(i)*fun(i);
}
__global__
void addSum(unsigned n,float *a,float *b,float *c){
  unsigned i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i<n) {
    c[i]=a[i]+b[i];
    printf("a[%2d]:%5.3f, b[%2d]:%5.3f, c[%2d]:%5.3f \n",i,a[i],i,b[i],i,c[i]);
  }
}
int main(){
  unsigned nElem = 23;
  unsigned nBytes = nElem*sizeof(float);

  float *a_d=NULL,*b_d=NULL,*c_d=NULL;//device variable
 
  //Host-memory allocate array 
  std::valarray<float> a(nElem),b(nElem),c(nElem);

  // allocate memory on device 
	cudaMalloc(&a_d, nBytes);
	cudaMalloc(&b_d, nBytes);
	cudaMalloc(&c_d, nBytes);
  
  initData(sin,nElem,&a[0]);
  initData(cos,nElem,&b[0]);

  //copy infomation from host to device
  cudaMemcpy(a_d, &a[0], nBytes, cudaMemcpyHostToDevice);
  cudaMemcpy(b_d, &b[0], nBytes, cudaMemcpyHostToDevice);

  unsigned blockSize = 16;//warp size
  unsigned  gridSize = (nElem-1)/blockSize + 1;

  //kernel parallel summation on GPU
  addSum<<<gridSize,blockSize>>>(nElem,a_d,b_d,c_d);

  //copy infomation from device to host
  cudaMemcpy(&c[0], c_d, nBytes, cudaMemcpyDeviceToHost);

  for(unsigned i=0;i<nElem;i++){
    if (c[i] != 1.0) {
      printf("The data in [%d] doesn't match\n",i);
    }
  }

  
  cudaFree(a_d);
  cudaFree(b_d);
  cudaFree(c_d);

}
  
