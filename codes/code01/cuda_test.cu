#include<stdio.h>
__global__
void Hi(void){
  printf("I'm a thread %d in the block %d\n",threadIdx.x,blockIdx.x);
}
int main(void) {
  int numBlocks  = 1; //numeros de bloques
  int threadBlck = 10;  //hebras por bloques
  Hi<<<numBlocks,threadBlck>>>();
  cudaDeviceSynchronize();
  printf("That's all!\n");
  return(0);
}

