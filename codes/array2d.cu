//Cuda Kernel to add the elements of two arrays-2D on GPU
__global__ void add(int nx, int nz, float *x, float *y)
{ 
  //The idea is the each thread gets its index by computing the offset
  //to the beginning of its block (the block index times the block size:
  //blockIdx.x * blockDim.x) and adding the thread's index within the
  //block (threadIdx.x)
  dim3 index,stride;
  index.x  = blockIdx.x * blockDim.x + threadIdx.x; 
  index.y  = blockIdx.y * blockDim.y + threadIdx.y;
  stride.x = blockDim.x * gridDim.x; 
  stride.y = blockDim.y * gridDim.y;
  //Begin Grid-Stride loop in i
  for (int i=index.y; i<nx; i+=stride.y) {
    //Begin Grid-Stride loop in j
    for (int j=index.x; i<nz; j+=stride.x) {
      y[i*nz+j] =  x[i*nz+j] + y[i*nz+j]; //row-major order
    }//End Grid-Stride loop in j
  }//End Grid-Stride loop in i
}

